package CommonFunctions;

import com.jayway.jsonpath.JsonPath;
import io.restassured.specification.RequestSpecification;
import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.request;

public class APIMethod{

    RequestSpecification request = given();
    Response response;

    public void setUpRequest(){
        String URI = "https://www.thesportsdb.com";
        request.baseUri(URI);
        request.basePath("/api/v1/json/2");
    }

    public void methodGET(String endpoint){
        request.get(endpoint);
        response =request.get(endpoint);
    }

    public void verifyStatusCode(int statusCode){
        response.then().statusCode(statusCode);
    }

    public List<String> jsonData(String jsonPath){
        List<String> jsonData =JsonPath.parse(response.asString()).read(jsonPath);
        return jsonData;
    }
}


