package TestRunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/main/resources/TestAPI.feature",
        glue = {"StepsDefinition"}
)
public class TestRunner extends AbstractTestNGCucumberTests {

}
