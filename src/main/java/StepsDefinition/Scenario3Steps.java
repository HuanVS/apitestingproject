package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import CommonFunctions.APIMethod;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario3Steps {

    APIMethod apiMethod = new APIMethod();

    @When("Create a request API will be return all sports with endpoint")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("Verify status code is {int} and include some sport name:")
    public void statusCodeResponseIsAndWillDisplayedLeagues(int statusCode, DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$.sports..strSport");
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("sportName");
            Assert.assertTrue(jsonData.toString().contains(value));
        }
    }

}
