package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import CommonFunctions.APIMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario4Steps {
    APIMethod apiMethod = new APIMethod();

    @When("Create a request API will be return all Leagues in a country with endpoint")
    public void createARequestAPIWillBeReturnAllLeaguesInACountryWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("Verify status code is {int} and include some country:")
    public void verifyStatusCodeIsAndWillDisplayLeaguesInclude(int statusCode, DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        //verify response include some country
        List<String> jsonData2 = apiMethod.jsonData("$.countries..name_en");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        for (int i = 0; i < dataTable.height() - 1; i++) {
            String value = data.get(i).get("country");
            Assert.assertTrue(jsonData2.contains(value));

        }
    }

}
