package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import CommonFunctions.APIMethod;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario1Steps {

    APIMethod apiMethod = new APIMethod();

    @When("Create a request API to thesportsdb with endpoint:")
    public void makeRequestGET(DataTable dataTable){
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        String endpoint =data.get(0).get("endpoint");
        apiMethod.setUpRequest();
        apiMethod.methodGET(endpoint);
    }

    @Then("Verify status code response is {int} and include some tags:")
    public void verifyStatusCodeAndItem(int statusCode ,DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$.countries");
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("tags");
            Assert.assertTrue(jsonData.toString().contains(value));
        }
    }
}
