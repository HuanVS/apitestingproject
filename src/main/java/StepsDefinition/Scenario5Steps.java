package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import CommonFunctions.APIMethod;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario5Steps {
    APIMethod apiMethod = new APIMethod();

    @When("Create a request API will be return all player with endpoint")
    public void createARequestAPIWillBeReturnAllPlayerWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("Verify status code is {int} and will be display {int} player include some tags:")
    public void verifyStatusCodeIsAndWillBeDisplayPlayerIncludeSomeTags(int statusCode, int numberOfPlayer, DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$.players..idEdit");
        Assert.assertEquals(jsonData.size(),numberOfPlayer);
        List<String> jsonData2 = apiMethod.jsonData("$.players");
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String value = data.get(i).get("tagOfPlayer");
            Assert.assertTrue(jsonData2.toString().contains(value));
        }
    }
}
