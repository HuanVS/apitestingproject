package StepsDefinition;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import CommonFunctions.APIMethod;
import io.cucumber.java.en.When;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class Scenario2Steps {

    APIMethod apiMethod = new APIMethod();

    @When("Create a request API will be return all leagues with endpoint:")
    public void createAGETRequestToTheWebsiteThesportsdbComWithEndpoint(DataTable dataTable) {
        List<Map<String,String>> data = dataTable.asMaps(String.class,String.class);
        apiMethod.setUpRequest();
        apiMethod.methodGET(data.get(0).get("endpoint"));
    }

    @Then("status code response is {int} and will display {int} leagues include:")
    public void statusCodeResponseIsAndWillDisplayedLeagues(int statusCode,int numberOfLeagues,DataTable dataTable) {
        apiMethod.verifyStatusCode(statusCode);
        List<String> jsonData = apiMethod.jsonData("$.leagues..strLeagueAlternate");
        System.out.println(jsonData);
        Assert.assertEquals(jsonData.size(),numberOfLeagues);
        List<Map<String,String>> data=dataTable.asMaps(String.class,String.class);
        for (int i=0;i<dataTable.height()-1;i++){
            String leagues = data.get(i).get("leagues");
            Assert.assertTrue(jsonData.toString().contains(leagues));
        }
    }
}
