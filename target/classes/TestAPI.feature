Feature: API Testing with Rest Assured

  Scenario: 01_Verify API return all Leagues in a country
    When Create a request API to thesportsdb with endpoint:
      | endpoint        |
      | /search_all_leagues.php?c=England |
    Then Verify status code response is 200 and include some tags:
      | tags             |
      | idLeague         |
      | strSport         |
      | strLeague        |
      | dateFirstEvent   |
      | strCountry       |

  Scenario: 02_Verify API will be return all leagues.
    When Create a request API will be return all leagues with endpoint:
      | endpoint         |
      | /all_leagues.php |
    Then status code response is 200 and will display 924 leagues include:
      | leagues                |
      | Liga MX                |
      | Serie B                |
      | Serie C                |
      | Europa League          |
      | Champions League       |

  Scenario: 03_Verify API will be return all sports.
    When Create a request API will be return all sports with endpoint
        | endpoint         |
        | /all_sports.php  |
    Then Verify status code is 200 and include some sport name:
      | sportName  |
      | Rugby      |
      | Tennis     |
      | Cricket    |
      | Cycling    |
      | Soccer     |

  Scenario: 04_Verify API will be return all Leagues in a country
    When Create a request API will be return all Leagues in a country with endpoint
    | endpoint           |
    | /all_countries.php |
    Then Verify status code is 200 and include some country:
    | country          |
    | Australia        |
    | Brazil           |
    | Canada           |
    | England          |
    | Spain            |

  Scenario: 05_Verify API will be return all player
    When Create a request API will be return all player with endpoint
    | endpoint |
    | /searchloves.php?u=zag |
    Then Verify status code is 200 and will be display 185 player include some tags:
    | tagOfPlayer |
    | idEdit      |
    | strUsername |
    | idPlayer    |
    | idTeam      |
    | date        |
